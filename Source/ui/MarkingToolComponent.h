/*
 *  MarkingToolComponent.h
 *  sdaProj
 *
 */

#ifndef H_MarkingToolComponent
#define H_MarkingToolComponent

#include "../JuceLibraryCode/JuceHeader.h"
#include "CommentGroupsTabbedComponent.h"
#include "../model/MarkingTool.h"

class MarkingToolComponent  :   public Component,
                                private ActionListener
{	
public:
	//==============================================================================
	MarkingToolComponent(ValueTree& model);
	~MarkingToolComponent();
	//==============================================================================
	void resized();
	void paint (Graphics &g);
    void actionListenerCallback (const String& message);
	//==============================================================================
private:
	//==============================================================================
    
    TextEditor comment;
    CommentGroupsTabbedComponent  tabs;
    
};




#endif // H_MarkingToolComponent