/*
 *  JuceWindow.cpp
 *  sdaProj
 *
 */

#include "MarkingToolWindow.h"
#include "MarkingToolComponent.h"

//==============================================================================
MarkingToolWindow::MarkingToolWindow(ValueTree& model) :    DocumentWindow(ProjectInfo::projectName,  // Set the text to use for the title
                                           Colours::azure,			  // Set the colour of the window
                                           DocumentWindow::allButtons,// Set which buttons are displayed
                                           true)					  // This window should be added to the desktop
			  
{
    setUsingNativeTitleBar(true);
    setResizable(true, true);
    
    centreWithSize(960, 720);
    setContentOwned(new MarkingToolComponent (model), false);
    
    
	setVisible(true);
    
}

MarkingToolWindow::~MarkingToolWindow()
{

}

void MarkingToolWindow::closeButtonPressed()
{

    JUCEApplication::getInstance()->systemRequestedQuit();
} 