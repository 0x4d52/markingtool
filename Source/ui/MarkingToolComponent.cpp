/*
 *  MarkingToolComponent.cpp
 *  sdaProj
 *
 */

#include "MarkingToolComponent.h"


MarkingToolComponent::MarkingToolComponent(ValueTree& model) : tabs (model)
{
    addAndMakeVisible (&tabs);
    tabs.setTabBarDepth (25);
    tabs.setCurrentTabIndex (0);
    tabs.addActionListener(this);
    
    comment.setMultiLine(true);
    comment.setReturnKeyStartsNewLine(true);
    addAndMakeVisible(&comment);
    
    
}
MarkingToolComponent::~MarkingToolComponent()
{
	
}

//==============================================================================
void MarkingToolComponent::resized()
{
    int commetEndHeight = 200;
    
    tabs.setBounds (0, 0, getWidth(), getHeight() - commetEndHeight);
	comment.setBounds (0, getHeight() - commetEndHeight, getWidth(), commetEndHeight);
}

void MarkingToolComponent::paint (Graphics &g)
{

}

void MarkingToolComponent::actionListenerCallback (const String& message)
{
    comment.insertTextAtCaret(message + " ");
}