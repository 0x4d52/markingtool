//
//  CommentComponent.h
//  MarkingTool
//
//  Created by tj3-mitchell on 25/04/2013.
//
//

#ifndef H_CommentComponent
#define H_CommentComponent

#include "../JuceLibraryCode/JuceHeader.h"

//sould be CommentComponent

class CommentComponent  :   public Component,
                            private Button::Listener
{
public:
	//==============================================================================
	CommentComponent(ValueTree commentTree_);
	~CommentComponent();
    
    ValueTree& getCommentTree()
    {
        return commentTree;
    }
	//==============================================================================
	void resized();
	void paint (Graphics &g);
    void buttonClicked (Button* button);
    
    /** Class for a listeners  */
	class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** */
        virtual void messsagePosted(const String& text) = 0;
        
        /** Called when the + button is pressed */
        virtual void addButtonPressed (CommentComponent* source) = 0;
        
        /** Called when the - button is pressed */
        virtual void removeButtonPressed (CommentComponent* source) = 0;
    };
    
    /** adds the listener to recieve bytes as they are recieved. Only one at a time at the moment. */
	void addListener(Listener* const listenerToAdd);
	
    /** removes the listener so that it will no londger recieve messages. */
	void removeListener(Listener* const listenerToRemove);
    
	//==============================================================================
private:
	//==============================================================================
	TextButton postButton;
    TextButton minusButton;
    TextButton plusButton;
    TextEditor comment;
    ValueTree commentTree;
    ListenerList<Listener> listeners;
    
};



#endif /* H_CommentComponent */
