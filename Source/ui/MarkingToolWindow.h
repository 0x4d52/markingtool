/*
 *  JuceWindow.h
 *  sdaProj
 *
 */

#ifndef H_MarkingToolWindow
#define H_MarkingToolWindow
#include "../JuceLibraryCode/JuceHeader.h"


class MarkingToolWindow  : public DocumentWindow
{
public:
	//==============================================================================
	MarkingToolWindow(ValueTree& tree);
	~MarkingToolWindow();
	//==============================================================================
	// called when the close button is pressed or esc is pushed
	void closeButtonPressed();
	
};



#endif //H_MarkingToolWindow