//
//  File.h
//  MarkingTool
//
//  Created by tj3-mitchell on 08/05/2013.
//
//

#ifndef __MarkingTool__CommentGroupsTabbedComponent__
#define __MarkingTool__CommentGroupsTabbedComponent__

#include "CommentGroupComponent.h"
#include "../model/CommentGroups.h"
#include "../model/CommentGroup.h"

class CommentGroupsTabbedComponent  :   public TabbedComponent,
                                        public ActionListener,
                                        public ActionBroadcaster,
                                        private ValueTree::Listener
                                
{
public:
    CommentGroupsTabbedComponent (ValueTree& tree);
    void refreshFromTree();
    void addCommentGroup();
    void removeCommentGroup (const int index);
    void popupMenuClickOnTab (int tabIndex, const String &tabName);
    void mouseUp (const MouseEvent& event);
    void actionListenerCallback (const String& message);
    
    //==============================================================================
    void valueTreePropertyChanged (ValueTree& tree, const Identifier& property);
    void valueTreeChildAdded (ValueTree& parent, ValueTree& addedChild);
    void valueTreeChildRemoved (ValueTree& parent, ValueTree& removedChild);
    void valueTreeChildOrderChanged (ValueTree& movedParent);
    void valueTreeParentChanged (ValueTree& tree);
    
private:
    ValueTree commentGroupsTree;
};

#endif /* defined(__MarkingTool__CommentGroupsTabbedComponent__) */
