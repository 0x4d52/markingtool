//
//  CommentGroups.h
//  MarkingTool
//
//  Created by tj3-mitchell on 07/05/2013.
//
//

#ifndef __MarkingTool__CommentGroups__
#define __MarkingTool__CommentGroups__

#include "../JuceLibraryCode/JuceHeader.h"

class CommentGroups
{
public:
    struct Ids
    {
        static const Identifier commentGroups;
    };
    //===============
    static ValueTree createCommentGroups();
    static ValueTree getOrCreateCommentGroupsTree (const ValueTree& tree);
    
//    static int getNumCommentGroups (const ValueTree& tree);
//    static int getCommentGroupIndex (const ValueTree& tree);
//    static ValueTree getCommentGroup (const ValueTree& tree, int index);
    //================

private:
    CommentGroups();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CommentGroups)
};

#endif /* defined(__MarkingTool__CommentTabModel__) */
