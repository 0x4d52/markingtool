//
//  Model.cpp
//  MarkingTool
//
//  Created by tj3-mitchell on 06/05/2013.
//
//

#include "MarkingTool.h"

const Identifier MarkingTool::Ids::model      ("MODEL");
const Identifier MarkingTool::Ids::appVersion ("appVersion");