//
//  MarkingTool.h
//  MarkingTool
//
//  Created by tj3-mitchell on 06/05/2013.
//
//

#ifndef __MarkingTool__MarkingTool__
#define __MarkingTool__MarkingTool__

#include "../JuceLibraryCode/JuceHeader.h"

class MarkingTool
{
public:
    struct Ids
    {
        static const Identifier model;
        static const Identifier appVersion;
    };
    
    static ValueTree createDefaultModel()
    {
        ValueTree modelTree (Ids::model);
        modelTree.setProperty (Ids::appVersion, ProjectInfo::versionNumber, nullptr);
        
        return modelTree;
    }
    
    static ValueTree getModelTree (const ValueTree& tree)
    {
        ValueTree parent (tree);
        
        while (! parent.hasType (Ids::model) && parent.isValid())
            parent = parent.getParent();
        
        return parent;
    }
    
private:
    MarkingTool();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MarkingTool)
};


#endif /* defined(__MarkingTool__MarkingTool__) */
