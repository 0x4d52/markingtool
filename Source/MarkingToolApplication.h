/*
 *  MarkingToolApplication.h
 *  sdaProj
 *
 */

#ifndef H_MarkingToolApplication
#define H_MarkingToolApplication

#include "../JuceLibraryCode/JuceHeader.h"
#include "ui/MarkingToolWindow.h"
#include "model/MarkingTool.h"
#include "FileIO.h"

//==============================================================================
class MarkingToolApplication  : public JUCEApplication
{
public:
    //==========================================================================
    MarkingToolApplication();
    ~MarkingToolApplication();
    //==========================================================================
    void initialise (const String& commandLine);
    void shutdown();
    //==========================================================================
    void systemRequestedQuit();
    //==========================================================================
    const String getApplicationName();
    const String getApplicationVersion();
    bool moreThanOneInstanceAllowed();
    void anotherInstanceStarted(const String& commandLine);
	
private:
    ValueTree modelTree;
    MarkingToolWindow* markingToolWindow;
};


#endif //H_MarkingToolApplication